jQuery(document).ready(function ($) {
    // block appearance in section 3
    var heightHeader = $('header').get('0').offsetHeight;
    var heightPage_1 = $(".Page-1").get('0').offsetHeight;
    var heightPage_2 = $(".Page-2").get('0').offsetHeight;
    var heightPage_3 = $(".Page-3").get('0').offsetHeight;
    var allHeightBeforePage_3 = heightHeader + heightPage_1 + heightPage_2 - heightPage_3;
    var allHeightAfterPage_3 = heightHeader + heightPage_1 + heightPage_2 + heightPage_3;
    $(document).scroll(function () {
        var scroll = $(document).scrollTop();
        if (scroll >= allHeightBeforePage_3 && scroll < allHeightAfterPage_3) {
            $('.Blockchain-powered-text, .page-3-par-2-big-text').animate({
                opacity: 1
            }, 1500);
        }
    });
    // Function for section 5 "RoadMap"
    function init() {
        sec = 0;
        setInterval(tick, 1000);
    }
    function tick() {
        if (sec != 28) {
            sec++;
            if (sec % 3 == 0) {
                var numVisibility = sec / 3;
                $('.roadmap-text-' + numVisibility + '').animate({
                    opacity: 1
                }, 1000);
                $('.RM-circle-' + numVisibility + '').css('background-color', '#3ec2ff');
                $('.RM-line-vertical-' + numVisibility + '').css('background-color', '#3ec2ff');
            }
        } else {
            $('.roadmap-text').css('opacity', 0);
            $('.RM-line-vertical, .RM-circle').css('background-color', '#1f6595');
            sec = 0;
        }
    }
    init();
});